﻿using Medibra.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibra.Dal
{    
    public class DataContext : DbContext
    {
        public DbSet<Book> Books { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Film> Films { get; set; }
        public DbSet<Music> Music { get; set; }

        static DataContext()
        {
            Database.SetInitializer<DataContext>(new DbInitializer());
        }

        // class DbInitializer : DropCreateDatabaseIfModelChanges<DataContext> //
        class DbInitializer : DropCreateDatabaseAlways<DataContext>
        {
            protected override void Seed(DataContext context)
            {
                //seed here
                context.Books.Add(
                    new Book
                    {
                        Name = "Gone Girl",
                        Author = "Gillian Flynn"
                        //Currency = Media.Currencies.Euro
                        // Price = 6.95
                    });

                base.Seed(context);
            }
        }
    }
}
