﻿using Medibra.Dal;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Medibra.Repo
{
    public abstract class Repository<T> : IDisposable where T : class
    {
        protected DataContext _dbContext;

        public Repository()
        {
            _dbContext = new DataContext();
        }

        public Repository(DataContext context)
        {
            _dbContext = context;
        }

        /// <summary>
        /// Gets all objects from database.
        /// </summary>
        public virtual IEnumerable<T> GetAll()
        {
            return _dbContext.Set<T>().ToArray();
        }

        /// <summary>
        /// Gets all filtered objects from database.
        /// </summary>
        public virtual IEnumerable<T> GetAll(Expression<Func<T, bool>> filter)
        {
            return _dbContext.Set<T>().Where<T>(filter);
        }

        /// <summary>
        /// Deletes entity
        /// </summary>
        public virtual void Delete(T entity)
        {
            _dbContext.Set<T>().Remove(entity);
            SaveChanges();
        }

        /// <summary>
        /// Deletes list of entities
        /// </summary>
        public virtual void Delete(List<T> entities)
        {
            _dbContext.Set<T>().RemoveRange(entities);
            SaveChanges();
        }

        /// <summary>
        /// Adds entity
        /// </summary>
        public virtual void Add(T entity)
        {
            _dbContext.Set<T>().Add(entity);
            SaveChanges();
        }

        /// <summary>
        /// Adds list of entities
        /// </summary>
        public virtual void Add(List<T> entities)
        {
            _dbContext.Set<T>().AddRange(entities);
            SaveChanges();
        }

        /// <summary>
        /// Saves all changes in the databse
        /// </summary>
        /// <returns>Returns 1 if successful 0 otherwise</returns>
        public virtual int SaveChanges()
        {
            return _dbContext.SaveChanges();
        }

        /// <summary>
        /// Disposes Datacontext
        /// </summary>
        public void Dispose()
        {
            if (_dbContext != null)
                _dbContext.Dispose();
        }
    }
}
