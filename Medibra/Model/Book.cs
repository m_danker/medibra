﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Medibra.Model
{
    public class Book : MediaBase
    {        
        [Required]
        [StringLength(100)]
        [Display(Name="Titel")]
        public override String Name { get; set; }

        //[Required]
        [StringLength(100)]
        [Browsable(false)]
        public String Author { get; set; }

        [StringLength(13)]
        public String ISBN13 { get; set; }

        [MaxLength]
        [Display(Name="Cover")]
        public override byte[] Image { get; set; }
    }
}
