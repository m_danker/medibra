﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Medibra.Model
{
    public class Film : MediaBase
    {
        [Required]
        [StringLength(30)]
        [Display(Name = "Title")]
        public override String Name { get; set; }
    }
}
