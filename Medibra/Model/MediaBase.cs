﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Medibra.Model
{
    public abstract class MediaBase : Item
    {
        public enum Currencies { Euro, Dollar }

        [Key]
        [Browsable(false)]
        public int Id { get; set; }

        [EnumDataType(typeof(Currencies))]
        public Currencies? Currency { get; set; }

        [DataType(DataType.Currency)]
        public double? Price { get; set; }

        [Range(0,5)]
        [Browsable(false)]
        [Display(AutoGenerateField = false)]
        public int Rating { get; set; }

        [MaxLength]
        public virtual byte[] Image { get; set; }

        public virtual String Name { get; set; }

        [StringLength(500)]
        public virtual String Description { get; set; }

        [StringLength(500)]
        public String Comment { get; set; }

        [DataType(DataType.Date)]
        public DateTime? ReleaseDate { get; set; }
    }
}
