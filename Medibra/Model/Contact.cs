﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Medibra.Model
{
    public class Contact : Item
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name="Name")]
        public String Surname { get; set; }

        [Required]
        [StringLength(30)]
        [Display(Name = "Vorname")]
        public String Forename { get; set; }

        [DataType(DataType.Date)]
        public Address Address { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime? Birthday { get; set; }

        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public String PrivatePhoneNumber { get; set; }

        [StringLength(20)]
        [DataType(DataType.PhoneNumber)]
        public String ProfessionalPhoneNumber { get; set; }

        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public String PrivateEmailAddress { get; set; }

        [StringLength(50)]
        [DataType(DataType.EmailAddress)]
        public String ProfessionalEmailAddress { get; set; }
    }

    public class Address
    {
        [StringLength(30)]
        public String City { get; set; }

        [DataType(DataType.PostalCode)]
        public String Postalcode { get; set; }

        [StringLength(50)]
        public String StreetName { get; set; }

        [StringLength(5)]        
        public String StreetNumber { get; set; }
    }
}
