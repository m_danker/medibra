﻿using Medibra.Model;
using Medibra.Repo;
using System.Collections.ObjectModel;


namespace Medibra.ViewModel
{
    public class MainViewModel
    {
        public ObservableCollection<Item> SelectedCollection { get; set; }
        public ObservableCollection<Book> BookCollection { get; set; }
        public ObservableCollection<Contact> ContactCollection { get; set; }
        public ObservableCollection<Film> FilmCollection { get; set; }
        public ObservableCollection<Music> MusicCollection { get; set; }

        public MainViewModel()
        {
            BookCollection = new ObservableCollection<Book>(new BookRepository().GetAll());
            ContactCollection = new ObservableCollection<Contact>(new ContactRepository().GetAll());
            FilmCollection = new ObservableCollection<Film>(new FilmRepository().GetAll());
            MusicCollection = new ObservableCollection<Music>(new MusicRepository().GetAll());

            SelectedCollection = BookCollection;
        }

    }
}
